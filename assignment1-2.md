# Assignment 1-2 #

เขียนโปรแกรมแปลงตัวเลขเป็นคำภาษาอังกฤษ

### Input Domain ###
{x: Integer | 0 <= x <= 10}

### Example ###
Input     | Output
:---------| :---------
1         | One
5         | Five
10        | Ten

# Basic Swift Assignment #

รวมโจทย์ทบทวนตวามเข้าใจในเนื้อหาของหนังสือ The Swift Programming Language (Swift 4) ในบท A Swift Tour

### How to do assignment ###

* การกำหนด input ของโปรแกรม จะกำหนดโดยการประกาศค่าคงไว้บนสุดของโค้ด เช่น let input = 100
* การตั้งชื่อไฟล์ assignment ให้เขียนคำว่า assignment ตามด้วยเลขต่อท้าย เช่น assignment1-1.swift

### Assignments ###

* Simple value
    + [Assignment 1.1](https://bitbucket.org/dkver1/basic-swift-assignment/src/9d84091b88bdc43eda3494f6971eb7b25aaab769/assignment1-1.md?at=master&fileviewer=file-view-default)
    + [Assignment 1.2](https://bitbucket.org/dkver1/basic-swift-assignment/src/a2d584308a33dac0d2f378440cb4e35d37595539/assignment1-2.md?at=master&fileviewer=file-view-default)
    + [Assignment 1.3](https://bitbucket.org/dkver1/basic-swift-assignment/src/a2d584308a33dac0d2f378440cb4e35d37595539/assignment1-3.md?at=master&fileviewer=file-view-default)
    
* Control flow: if-else
    + [Assignment 2.1](https://bitbucket.org/dkver1/basic-swift-assignment/src/af4fa9adbb99c2df3ee9fc9acab614bebac927f2/assignment2-1.md?at=master&fileviewer=file-view-default)
    + [Assignment 2.2](https://bitbucket.org/dkver1/basic-swift-assignment/src/56ff2529d3ad83d14532eff0217c28964f9f5c3d/assignment2-2.md?at=master&fileviewer=file-view-default)
    + [Assignment 2.3](https://bitbucket.org/dkver1/basic-swift-assignment/src/af4fa9adbb99c2df3ee9fc9acab614bebac927f2/assignment2-3.md?at=master&fileviewer=file-view-default)

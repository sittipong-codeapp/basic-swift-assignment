# Assignment 1-3 #

เขียนโปรแกรมหาผลรวมของจำนวนนับ 2 จำนวน ที่เขียนเป็นคำภาษาอังกฤษ

### Input Domain ###
{x: String, y: String | 0 <= numberOf(x) + numberOf(y) <= 21}

### Example ###
Input     | Output
:---------| :---------
One\Two   | 3
Ten\One   | 11
Five\Five | 10

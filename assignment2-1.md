# Assignment 2-1 #

โปรแกรมคำนวณตัวเลขภาษาอังกฤษ 2 จำนวน (บวก ลบ คูณ หาร)

### Input Domain ###
{x: String, y: String | 0 <  numberOf(x), numberOf(y) < 10}

### Example ###
Input     | Output
:---------| :---------
One/+/One | 2
One/-/One | 0
One/*/One | 1
One/\/One | 1

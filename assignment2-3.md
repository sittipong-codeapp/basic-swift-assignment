# Assignment 2-3 #

เขียนโปรแกรมเช็คตัวอักษรภาษาอังกฤษที่เข้ามาว่าเป็นสระหรือไม่

### Input Domain ###
a-z, A-z

### Example ###
Input     | Output
:---------| :---------
C         | not vowel
A         | vowel

